#! /bin/bash

key1=$(cat ./ram/key1 | hexdump -ve'1/1 "%02x"')
key2=$(cat ./ram/key2 | hexdump -ve'1/1 "%02x"')
keyFinal=`echo $((0x$key1 ^ 0x$key2)) | base64`

openssl enc -aes-128-cbc -pbkdf2 -k $keyFinal -in banque -out banque.enc

rm -rf ram/* banque
