#! /bash/bin

mkdir "USB1"
mkdir "USB2"
mkdir "ram"
touch "banque"

dd if=/dev/urandom bs=16 count=1 of=./ram/key1
dd if=/dev/urandom bs=16 count=1 of=./ram/key2

echo "Entrez le premier mot de passe:"
read mdp1
mdp1=$(echo $mdp1 | sha256sum)
openssl enc -aes-128-cbc -pbkdf2 -k $mdp1 -in ./ram/key1 -out ./USB1/key1.enc

echo "Entrez le second mot de passe:"
read mdp2
mdp2=$(echo $mdp2 | sha256sum)
openssl enc -aes-128-cbc -pbkdf2 -k $mdp2 -in ./ram/key2 -out ./USB2/key2.enc

key1=$(cat ./ram/key1 | hexdump -ve'1/1 "%02x"')
key2=$(cat ./ram/key2 | hexdump -ve'1/1 "%02x"')
keyFinal=`echo $((0x$key1 ^ 0x$key2)) | base64`

openssl enc -aes-128-cbc -pbkdf2 -k $keyFinal -in banque -out banque.enc

rm -rf ram/* banque
