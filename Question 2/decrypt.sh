#! /bin/bash

echo "Entrez le premier mot de passe:"
read mdp1
mdp1=$(echo $mdp1 | sha256sum)
openssl enc -d -aes-128-cbc -pbkdf2 -k $mdp1 -in ./USB1/key1.enc -out ./ram/key1

echo "Entrez le second mot de passe:"
read mdp2
mdp2=$(echo $mdp2 | sha256sum)
openssl enc -d -aes-128-cbc -pbkdf2 -k $mdp2 -in ./USB2/key2.enc -out ./ram/key2

key1=$(cat ./ram/key1 | hexdump -ve'1/1 "%02x"')
key2=$(cat ./ram/key2 | hexdump -ve'1/1 "%02x"')
keyFinal=`echo $((0x$key1 ^ 0x$key2)) | base64`

openssl enc -d -aes-128-cbc -pbkdf2 -k $keyFinal -in banque.enc -out banque
