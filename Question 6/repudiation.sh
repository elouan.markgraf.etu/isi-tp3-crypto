#! /bin/bash

bash decrypt.sh

echo "Voulez-vous supprimer Personne A ou B ? (A / B)"
read personne

echo "Voulez-vous supprimer le Responsable ou le Représentant ? (Resp / Repr)"
read poste

echo "Entrez le nouveau mot de passe:"
read mdp
mdp=$(echo $mdp | sha256sum)

if [ $personne == "A" ]
then
        if [ $poste == "Resp" ]
        then
                rm ./USB1/key1Res.enc
                openssl enc -aes-128-cbc -pbkdf2 -k $mdp -in ./ram/key1 -out ./USB1/key1Res.enc
        else
                rm ./USB1/key1Rep.enc
                openssl enc -aes-128-cbc -pbkdf2 -k $mdp -in ./ram/key1 -out ./USB1/key1Rep.enc
        fi
else
        if [ $poste == "Resp" ]
        then
                rm ./USB2/key2Res.enc
                openssl enc -aes-128-cbc -pbkdf2 -k $mdp -in ./ram/key2 -out ./USB2/key2Res.enc
        else
                rm ./USB2/key2Rep.enc
                openssl enc -aes-128-cbc -pbkdf2 -k $mdp -in ./ram/key2 -out ./USB2/key2Res.enc
        fi
fi

bash crypt.sh
