#! /bash/bin

mkdir "USB1"
mkdir "USB2"
mkdir "ram"
touch "banque"

dd if=/dev/urandom bs=16 count=1 of=./ram/key1
dd if=/dev/urandom bs=16 count=1 of=./ram/key2

echo "Entrez le premier mot de passe (Responsable):"
read mdp1Res
mdp1Res=$(echo $mdp1Res | sha256sum)
openssl enc -aes-128-cbc -pbkdf2 -k $mdp1Res -in ./ram/key1 -out ./USB1/key1Res.enc

echo "Entrez le premier mot de passe (Représentant):"
read mdp1Rep
mdp1Rep=$(echo $mdp1Rep | sha256sum)
openssl enc -aes-128-cbc -pbkdf2 -k $mdp1Rep -in ./ram/key1 -out ./USB1/key1Rep.enc

echo "Entrez le second mot de passe (Responsable):"
read mdp2Res
mdp2Res=$(echo $mdp2Res | sha256sum)
openssl enc -aes-128-cbc -pbkdf2 -k $mdp2Res -in ./ram/key2 -out ./USB2/key2Res.enc

echo "Entrez le second mot de passe (Représentant):"
read mdp2Rep
mdp2Rep=$(echo $mdp2Rep | sha256sum)
openssl enc -aes-128-cbc -pbkdf2 -k $mdp2Rep -in ./ram/key2 -out ./USB2/key2Rep.enc

key1=$(cat ./ram/key1 | hexdump -ve'1/1 "%02x"')
key2=$(cat ./ram/key2 | hexdump -ve'1/1 "%02x"')
keyFinal=`echo $((0x$key1 ^ 0x$key2)) | base64`

openssl enc -aes-128-cbc -pbkdf2 -k $keyFinal -in banque -out banque.enc

rm -rf ram/* banque
