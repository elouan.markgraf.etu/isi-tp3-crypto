**Question 1:**

La solution de déchiffrement par l'authentification à deux facteurs des deux responsables se déroule ainsi: \
Pour chaque responsable :
- On entre le mot de passe du responsable, ensuite on applique une fonction de hachage dessus.
- On récupère la clé qu'il y a sur la clé USB et on la décrypte avec le mot de passe haché.
- On effectue l'opération XOR sur les 2 nouvelles clés décryptées des responsables, ce qui nous donne la clé pour accéder au fichier.
<img src="Schema1.png">

**Question 2:** Code dans le dossier Question 2.

**Question 3:**

Durant la mise en service, au lieu d'initialisé un chiffrement par clé USB, nous allons initialisé deux chiffrements par clé USB correspondant au responsable et au représentant de chaque clé USB.
Chacun de ces chiffrements sera stocké comme précédemment sur les clé USB respectives.
Ensuite lorsque nous demanderons une identification, nous demanderons si la personne qui va rentrer son mot de passe, est le responsable de la clé USB (sinon il s'agira du représentant).
En fonction de la réponse, nous utiliserons le chiffrement de la clé USB qu'il faut.
<img src="Schema2.png">

**Question 4:** Code dans le dossier Question 4.

**Question 5:**

Pour effectuer une répudiation après la mise en service, il nous suffira de supprimer le fichier de chiffrement correspondant à un responsable ou à un représentant dans la clé USB dont ils ont la responsabilité.
Puis ensuite, on ajoute une nouvelle personne avec un nouveau mot de passe et on crypte une nouvelle clé avec celui-ci, pour remplacer la personne supprimée.

**Question 6:** Code dans le dossier Question 6.
